﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Command;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Controllers
{
    [Route("api/")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IMediator _Imediator;
        public HomeController(IMediator m)
        {
            _Imediator = m;
        }

        [HttpGet]
        [Route("get-all-product")]
        public async Task<IEnumerable<Product>> Get()
        {
            return await _Imediator.Send(new GetAllProductQuery());
        }

        [HttpGet]
        [Route("get-proudct-by-id")]
        public async Task<Product> Get(int id)
        {
            return await _Imediator.Send(new GetProductByIdQuery() { ProudctId = id });
        }

        [HttpGet]
        [Route("get-order")]

        public async Task<IEnumerable<ProductOrder>> GetOrder(int userId)
        {
            return await _Imediator.Send(new GetOrderInfoQuery() { UserId = userId });
        }

        [HttpGet]
        [Route("get-category")]

        public async Task<IEnumerable<MenuBar>> GetCategory()
        {
            return await _Imediator.Send(new GetCategoryQuery());
        }
        [HttpPost]
        [Route("add-product-order")]

        public async Task<ProductOrder> AddProductOrder(int productid, int userid)
        {
            return await _Imediator.Send(new AddProductCommand() { ProudctId = productid, UserId = userid });
        }
        

    }
}
