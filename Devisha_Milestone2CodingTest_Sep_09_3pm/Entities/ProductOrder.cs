﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Entities
{
    public partial class ProductOrder
    {
        [Required]
        public int OrderId { get; set; }
        [Required]
        public int? ProductId { get; set; }
        [Required]
        public int? UserId { get; set; }
        [Required]
        public virtual Product Product { get; set; }
        [Required]
        public virtual ApplicationUser User { get; set; }
    }
}
