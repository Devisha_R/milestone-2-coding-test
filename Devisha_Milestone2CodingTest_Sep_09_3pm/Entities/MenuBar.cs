﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Entities
{
    public partial class MenuBar
    {
        [Required]
        [Range(3, 50)]
        public int Id { get; set; }
        [Required]
        [Range(3, 50)]
        public string Title { get; set; }
        [Required]
        [Range(3, 50)]
        public string Url { get; set; }
        [Required]
        public bool? OpenInNewWindow { get; set; }
    }
}
