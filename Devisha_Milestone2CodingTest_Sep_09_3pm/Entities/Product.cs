﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Entities
{
    public partial class Product
    {
        public Product()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }
        [Required]
        [Range(3, 50)]
        public int ProudctId { get; set; }
        [Required]
        [Range(3, 50)]
        public string ProductName { get; set; }
        [Required]
        [Range(3, 50)]
        public string Description { get; set; }
        [Required]
        [Range(3, 50)]
        public decimal? Amount { get; set; }
        [Required]
        [Range(3, 50)]
        public int? Stock { get; set; }
        [Required]
        [Range(3, 50)]
        public int? CatId { get; set; }
        [Required]
        [Range(3, 50)]
        public string Photo { get; set; }

        public virtual Categories Cat { get; set; }
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
