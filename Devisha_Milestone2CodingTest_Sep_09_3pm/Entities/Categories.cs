﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Entities
{
    public partial class Categories
    {
        public Categories()
        {
            Product = new HashSet<Product>();
        }
        [Required]
        public int CatId { get; set; }
        [Required]
        public string CatName { get; set; }
        [Required]
        public virtual ICollection<Product> Product { get; set; }
    }
}
