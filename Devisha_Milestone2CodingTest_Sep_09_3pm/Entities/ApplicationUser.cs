﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Entities
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }
        [Required]
        public int UserId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public long? MobileNumber { get; set; }
        [Required]
        public int? PinCode { get; set; }
        [Required]
        [Range(4,50)]
        public string HouseNoBuildingName { get; set; }
        [Required]
        [Range(4, 50)]
        public string RoadArea { get; set; }
        [Required]
        [Range(4, 50)]
        public string City { get; set; }
        [Required]
        [Range(4, 50)]
        public string State { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
