﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using MediatR;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Command
{
    public class AddProductCommand : IRequest<ProductOrder>
    {
        public int ProudctId { get; set; }
        public int UserId { get; set; }
    }
}

