﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using MediatR;
using System.Collections.Generic;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Queries
{
    public class GetAllProductQuery : IRequest<IEnumerable<Product>>
    {
    }
}

