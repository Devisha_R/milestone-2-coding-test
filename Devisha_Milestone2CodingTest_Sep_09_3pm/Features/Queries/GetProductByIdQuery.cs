﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using MediatR;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Queries
{
    public class GetProductByIdQuery : IRequest<Product>
    {
        public int ProudctId { get; set; }
    }
}


