﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Persistence;
using System.Collections.Generic;
using System.Linq;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Repository
{
    public class GroceryServices:IGroceryServices
    {
        private readonly GroceriesDbContext _context;
        public GroceryServices(GroceriesDbContext data)
        {
            _context = data;
        }

        public ProductOrder AddProductOrder(int pid, int uid)
        {
            var orderid = _context.ProductOrder.Max(x => x.OrderId) + 1;
            var product = _context.Product.SingleOrDefault(x => x.ProudctId == pid);
            var user = _context.ApplicationUser.SingleOrDefault(x => x.UserId == uid);
            var product_order = new ProductOrder()
            {
                UserId = uid,
                ProductId = pid,
                OrderId = orderid,
                Product = product,
                User = user
            };
            _context.ProductOrder.Add(product_order);
            _context.SaveChanges();
            return product_order;

        }

        public IEnumerable<Product> GetAllProduct()
        {


            return _context.Product.ToList();
        }

        public IEnumerable<MenuBar> GetCategory()
        {
            return _context.MenuBar.ToList();
        }

        public Product GetProductById(int ProductId)
        {
            var data = _context.Product.FirstOrDefault(p => p.ProudctId == ProductId);
            return data;
        }

        public IEnumerable<ProductOrder> OrderInfo(int UserId)
        {
            var data = _context.ProductOrder.Where(p => p.UserId == UserId);
            return data.ToList();
        }
    }
}

