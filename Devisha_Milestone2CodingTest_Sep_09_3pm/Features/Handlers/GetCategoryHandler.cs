﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Persistence;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Handlers
{
    public class GetCategoryHandler : IRequestHandler<GetCategoryQuery, IEnumerable<MenuBar>>
    {
        private readonly IGroceryServices _data;
        public GetCategoryHandler(IGroceryServices data)
        {
            _data = data;
        }
        public Task<IEnumerable<MenuBar>> Handle(GetCategoryQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.GetCategory());

        }
    }
    }
