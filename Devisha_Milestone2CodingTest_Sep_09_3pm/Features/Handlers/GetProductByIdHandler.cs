﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Queries;
using MediatR;
using System.Threading.Tasks;
using System.Threading;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Persistence;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, Product>
    {
        private readonly IGroceryServices _data;
        public GetProductByIdHandler(IGroceryServices data)
        {
            _data = data;
        }


        public Task<Product> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.GetProductById(request.ProudctId));
        }
    }
}