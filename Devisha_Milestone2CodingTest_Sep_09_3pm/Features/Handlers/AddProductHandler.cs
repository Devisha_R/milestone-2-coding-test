﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Command;
using MediatR;
using System.Threading.Tasks;
using System.Threading;
using Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Persistence;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Handlers
{
    public class AddProductHandler : IRequestHandler<AddProductCommand, ProductOrder>
    {
        private readonly IGroceryServices _data;
        public AddProductHandler(IGroceryServices data)
        {
            _data = data;
        }
        public async Task<ProductOrder> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.AddProductOrder(request.ProudctId, request.UserId));
        }
    }
}