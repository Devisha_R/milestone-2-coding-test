﻿using Devisha_Milestone2CodingTest_Sep_09_3pm.Entities;
using System.Collections.Generic;

namespace Devisha_Milestone2CodingTest_Sep_09_3pm.Features.Persistence
{
    public interface IGroceryServices
    {
        ProductOrder AddProductOrder(int productid, int userid);
        IEnumerable<Product> GetAllProduct();
        IEnumerable<MenuBar> GetCategory();

        Product GetProductById(int ProductId);
        IEnumerable<ProductOrder> OrderInfo(int UserId);
    }
}
